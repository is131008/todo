from django.shortcuts import render, HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.template import loader, Context
from django.template.loader import get_template
from django.contrib.auth.models import User

from www.todolist.models import TodoEntry
from www.todolist.form import new_form

# Create your views here.


@login_required(login_url='django.contrib.auth.views.login')
def overview(request):
    open_entries = TodoEntry.objects.filter(user_id=request.user)
    context = {'open_entries': open_entries}
    return render(request, 'overview.html', context)
# return render_to_response("overview.html",{"object_list":object_list})


@login_required(login_url='django.contrib.auth.views.login')
def add(request):
    if request.method == 'POST':
        form = new_form(request.POST)

        if form.is_valid():
            user = request.user
            title = request.POST.get('title', '')
            desc = request.POST.get('description', '')
            date = request.POST.get('due_date', '')
            time = request.POST.get('due_time', '')

            entry = TodoEntry(user_id=user, title=title,
                              description=desc,
                              due_date=date,
                              due_time=time)
            entry.save()

            return HttpResponseRedirect('overview')
        else:
            return HttpResponseRedirect('add')


# if a GET (or any other method) we'll create a blank form
    else:
        form = new_form()

    context = {'form': form, 'object': None}
    return render(request, 'forms.html', context)


@login_required(login_url='django.contrib.auth.views.login')
def edit(request, id):
    entry = get_object_or_404(TodoEntry, pk=id)
    if request.method == 'POST':
        form = new_form(request.POST)
        if form.is_valid():
            user = User.objects.get(id=1)
            title = request.POST.get('title', '')
            desc = request.POST.get('description', '')
            date = request.POST.get('due_date', '')
            time = request.POST.get('due_time', '')

            TodoEntry.objects.filter(pk=id).update(user_id=user, title=title,
                                                   description=desc,
                                                   due_date=date,
                                                   due_time=time)
            return HttpResponseRedirect('../overview')
        else:
            return HttpResponseRedirect('')
    else:
        form = new_form(instance=entry)
    context = {'form': form, 'entry': entry}
    return render(request, 'forms.html', context)


@login_required(login_url='django.contrib.auth.views.login')
def delete(request, id):
    entry = get_object_or_404(TodoEntry, pk=id)
    entry.delete()
    return redirect('overview')


@login_required(login_url='django.contrib.auth.views.login')
def switch(request, id):
    entry = get_object_or_404(TodoEntry, pk=id)
    entry.done = not entry.done
    entry.save()
    return redirect('overview')
