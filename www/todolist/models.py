from django.db import models

from django.contrib.auth.models import User

from datetime import date


class TodoEntry (models.Model):

    id = models.AutoField(primary_key=True)
    user_id = models.ForeignKey(User)
    title = models.CharField(max_length=50)
    description = models.CharField(max_length=500)
    due_date = models.DateField('Due date', null=True)
    due_time = models.TimeField('Due time', null=True)
    creation_datetime = models.DateTimeField('Creation Date and Time',
                                             default=date.today)
    done = models.BooleanField(default=False)
