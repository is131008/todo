import logging
from django.test import TestCase
from django.contrib.auth.models import User, AnonymousUser
from django.test.client import Client, RequestFactory
from django.core.urlresolvers import reverse, resolve
from www.todolist.models import TodoEntry
from www.todolist.views import overview, add, edit, switch, delete
from django.utils import timezone


def debug(obj):
        logging.basicConfig()
        log = logging.getLogger("log")


class TodoTestCase(TestCase):
    fixtures = ["testuser.json"]


class DatabaseTest(TestCase):
    def test_entries(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.assertEqual(User.objects.all().count(), 2)


class UrlTests(TestCase):
    def test_todolist_url_todolist(self):
        resolver = resolve('/todolist/overview')
        self.assertEqual(resolver.view_name, 'overview')

    def test_todolist_url_add(self):
        resolver = resolve('/todolist/add')
        self.assertEqual(resolver.view_name, 'add')

    def test_todolist_url_edit(self):
        resolver = resolve('/todolist/edit/3')
        self.assertEqual(resolver.view_name, 'edit')

    def test_todolist_url_switch(self):
        resolver = resolve('/todolist/switch/3')
        self.assertEqual(resolver.view_name, 'switch')

    def test_todolist_url_delete(self):
        resolver = resolve('/todolist/delete/3')
        self.assertEqual(resolver.view_name, 'delete')


class ViewTests(TestCase):
    def test_overview(self):
        response = self.client.get(reverse("overview"))
        self.assertEqual(response.status_code, 302)

    def test_add_view(self):
        response = self.client.get(reverse("add"))
        self.assertEqual(response.status_code, 302)

    def test_delete_view(self):
        response = self.client.get(reverse("logout"))
        self.assertEqual(response.status_code, 302)

    def test_edit_view(self):
        response = self.client.get(reverse("edit", kwargs={'id': 1}))
        self.assertEqual(response.status_code, 302)

    def test_switch_view(self):
        response = self.client.get(reverse("switch", kwargs={'id': 1}))
        self.assertEqual(response.status_code, 302)


class User_ViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='melisa',
                                             email='melisa@tests',
                                             password='melisa')

    def test_overview_testUser(self):
        request = self.factory.get('/todolist/overview')
        request.user = self.user
        response = overview(request)
        self.assertEqual(response.status_code, 200)

    def test_anonymousUser(self):
        request = self.factory.get('/todolist/overview')
        request.user = AnonymousUser()
        response = overview(request)
        self.assertEqual(response.status_code, 302)

    def test_addView_testUser_get(self):
        request = self.factory.get('/todolist/add')
        request.user = self.user
        response = add(request)
        self.assertEqual(response.status_code, 200)

    def test_addView_anonymousUser_get(self):
        request = self.factory.get('todolist/add')
        request.user = AnonymousUser()
        response = add(request)
        self.assertEqual(response.status_code, 302)

    def test_addView_testUser_get(self):
        request = self.factory.get('/todolist/add')
        request.user = self.user
        response = add(request)
        self.assertEqual(response.status_code, 200)

    def test_addView_anonymousUser_get(self):
        request = self.factory.get('todolist/add')
        request.user = AnonymousUser()
        response = add(request)
        self.assertEqual(response.status_code, 302)

    def test_addView_testUser_post(self):
        request = self.factory.post('/todolist/add')
        request.user = self.user
        response = add(request)
        self.assertEqual(response.status_code, 302)

    def test_addView_anonymousUser_post(self):
        request = self.factory.post('todolist/add')
        request.user = AnonymousUser()
        response = add(request)
        self.assertEqual(response.status_code, 302)

    def test_deleteView_anonymousUser_get(self):
        request = self.factory.get('todolist/delete')
        request.user = AnonymousUser()
        response = delete(request, 1)
        self.assertEqual(response.status_code, 302)

    def test_deleteView_anonymousUser_post(self):
        request = self.factory.post('todolist/delete')
        request.user = AnonymousUser()
        response = delete(request, 1)
        self.assertEqual(response.status_code, 302)


class LoginTest(TestCase):
    def test_login(self):
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        response = self.client.get(reverse("overview"))
        self.assertEqual(response.status_code, 200)


class InvalidLoginTest(TestCase):
    def test_invalidlogin(self):
        self.client = Client()
        self.client.login(username="testuser", password="test")
        response = self.client.get('/todolist/')
        self.assertEqual(response.status_code, 200)


class AddTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_add(self):
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        data = {
         "title": "Test",
        }
        response = self.client.post(reverse("add", data=data))
        self.assertEqual(response.status_code, 302)
        self.assertEqual(TodoEntry.objects.all().count(), 3)

    def test_csrf(self):
        self.client = Client(enforce_csrf_checks=True)
        self.client.login(username="user",
                          password="user")
        data = {
            'title': 'missing csrf token',
            'description': 'it does not work!',
            'due_date':  '2015-04-17',
            'due_time':  '12:00',
        }
        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(TodoEntry.objects.all().count(), 3)

    def test_add_success(self):
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        self.client.get(reverse("add"))

        data = {
            'title': 'successful',
            'description': 'it works!',
            'due_date':  '2015-04-17',
            'due_time':  '12:00'
        }

        response = self.client.post(reverse("add"), data=data)
        self.assertEqual(TodoEntry.objects.all().count(), 4)
        self.assertRedirects(response,
                             reverse('overview'),
                             status_code=302,
                             target_status_code=200,
                             msg_prefix='')
        self.assertEqual(TodoEntry.objects.all().count(), 4)


class TodoTestCase(TestCase):
    fixtures = ["testuser.json"]

    def test_fixture_insert(self):
        self.assertTrue(TodoEntry.objects.all().count())


class UrlTests(TestCase):

    def test_add_url(self):
        resolver = resolve('/todolist/add')
        self.assertEqual(resolver.view_name, 'add')


class LoginUserTest(TodoTestCase):
    def test_so(self):
        self.client = Client()
        self.user = User.objects.create_user(
            username='melisa', email='melisa@beispiel.com', password='geheim')
        self.client.login(username="melisa", password="geheim")
        response = self.client.get('/todolist/overview/login')
        self.assertEqual(response.status_code, 404)


class FixtureTests(TestCase):
    def test_entries(self):
        # check for fixtures data load
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        # check if fixture created 2 users
        self.assertEqual(User.objects.all().count(), 2)


class DeleteTest(TestCase):

    def test_delete_2(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="superuser",
                          password="superuser")
        response = self.client.get('/todolist/')
        self.client.get(reverse('delete', kwargs={'id': 1}))
        self.assertEqual(TodoEntry.objects.all().count(), 2)


class SwitchTest(TestCase):
    # setup method is called before each test

    def setUp(self):
        self.client = Client()

    def test_switch(self):
        self.assertEqual(TodoEntry.objects.all().count(), 3)
        self.client = Client()
        self.client.login(username="testuser",
                          password="testuser")
        response = self.client.get('/todolist/overview')
        self.assertTrue('open_entries' in response.context)
        self.assertFalse(len(response.context['open_entries']) == 2)
        self.client.get(reverse('switch', kwargs={'id': 2}))
        self.assertFalse(len(response.context['open_entries']) == 2)
