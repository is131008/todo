from django import forms
from www.todolist import models


class new_form(forms.ModelForm):
    class Meta:
        model = models.TodoEntry
        fields = ['title', 'description', 'due_date', 'due_time']
