# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models, migrations
from datetime import datetime
from django.contrib.auth.models import User


def test_values(apps, schema_editor):

    entry = apps.get_model('todolist', 'TodoEntry')
    userobj = apps.get_model('auth', 'user')

    User.objects.create_user('superuser', '131008@tests', 'superuser')
    user = userobj.objects.get(id=1)

    User.objects.create_user('testuser', 'testuser@tests', 'testuser')
    user = userobj.objects.get(id=1)

    entry(user_id=user, title='Erster Eintrag',
          description='description 1', due_date=datetime.now(),
          due_time=datetime.now()).save()
    entry(user_id=user, title='Zweiter Eintrag',
          description='description 2', due_date=datetime.now(),
          due_time=datetime.now()).save()
    entry(user_id=user, title='Dritter Eintrag',
          description='description 3', due_date=datetime.now(),
          due_time=datetime.now()).save()


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0002_todoentry_done'),
    ]

    operations = [
        migrations.RunPython(test_values),
    ]
