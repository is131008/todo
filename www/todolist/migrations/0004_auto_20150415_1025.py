# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('todolist', '0003_auto_20150412_0953'),
    ]

    operations = [
        migrations.AlterField(
            model_name='todoentry',
            name='due_time',
            field=models.TimeField(null=True, verbose_name=b'Due time'),
        ),
        migrations.AlterField(
            model_name='todoentry',
            name='id',
            field=models.AutoField(serialize=False, primary_key=True),
        ),
    ]
