# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(name='TodoEntry', fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False,
                                        auto_created=True,
                                        primary_key=True)),
                ('title', models.CharField(max_length=50)),
                ('description', models.CharField(max_length=500)),
                ('due_date', models.DateField(null=True,
                                              verbose_name=b'Due date')),
                ('due_time', models.DateField(null=True,
                                              verbose_name=b'Due time')),
                ('creation_datetime',
                    models.DateTimeField(
                                    default=datetime.date.today,
                                    verbose_name=b'Creation Date and Time')),
                ('user_id', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
